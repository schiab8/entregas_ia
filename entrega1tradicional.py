from simpleai.search import SearchProblem, breadth_first, depth_first, limited_depth_first, astar, greedy
from simpleai.search.viewers import BaseViewer

def resolver(metodo_busqueda):
    metodo_busqueda += '(DotaProblem(INITIAL),graph_search=True)' if metodo_busqueda != 'limited_depth_first' else '(DotaProblem(INITIAL),depth_limit = 10, graph_search=True)'
    return eval(metodo_busqueda)

'''(HERO, ENEMY_HERO, ENEMY_BASE)'''
INITIAL = ((0,0),(2,2),(4,4))

def is_adjacent(pos1,pos2):
    if abs(pos1[0]-pos2[0]) == 1 and pos1[1]==pos2[1]:
        return True
    elif pos1[0]==pos2[0] and abs(pos1[1]-pos2[1]) == 1:
        return True
    else:
        return False

class DotaProblem(SearchProblem):
    
    def is_goal(self, state):
        return state[1:] == (None, None)
    
    def heuristic(self, state):
        xHero, yHero = state[0]
        distance = 0
        if state[1] is not None:
            xEnemy, yEnemy = state[1]
            distance += abs(xHero-xEnemy)+abs(yHero-yEnemy)
        if state[2] is not None:
            xBase, yBase = state[2]
            distance = +abs(xHero-xBase)+abs(yHero-yBase)
        
        return distance
            
    def cost(self, state1, action, state2):
        return 1
         
    def actions(self, state):
        xHero, yHero = state[0]        
        action_list = []
        
        if xHero > 0 and (xHero-1,yHero) not in state[1:]: #MOVE_LEFT
            action_list.append('l')
        if xHero < 4 and (xHero+1,yHero) not in state[1:]: #MOVE_RIGHT
            action_list.append('r')
        
        if yHero > 0 and (xHero,yHero-1) not in state[1:]: #MOVE_DOWN
            action_list.append('d')
        if yHero < 4 and (xHero,yHero+1) not in state[1:]: #MOVE_UP
            action_list.append('u')
        
        if state[1] is not None:
            if is_adjacent(state[0],state[1]) == 1: #ATTACK ENEMY_ HERO
                action_list.append('aH')
        if state[2] is not None:
            if is_adjacent(state[0],state[2]): #ATTACK ENEMY_BASE
                action_list.append('aB')
        
        return action_list
    
    def result(self, state, action):
        xHero, yHero = state[0]
        
        if action == 'l': return ((xHero-1, yHero), state[1], state[2])
        
        elif action == 'r': return ((xHero+1, yHero), state[1], state[2])
        
        elif action == 'u': return ((xHero, yHero+1), state[1], state[2])
        
        elif action == 'd': return ((xHero, yHero-1), state[1], state[2])
        
        elif action == 'aH': return (state[0], None, state[2])
        
        elif action == 'aB': return (state[0], state[1], None)
