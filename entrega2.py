# -*- coding: utf-8 -*-
import itertools
from simpleai.search import (CspProblem, backtrack,
                             MOST_CONSTRAINED_VARIABLE,
                             LEAST_CONSTRAINING_VALUE,
                             HIGHEST_DEGREE_VARIABLE,
                             min_conflicts)
def resolver(metodo):
    problem = CspProblem(variables, dominios, restricciones)
    if metodo == 'min_conflicts':
        return min_conflicts(problem, iterations_limit=1000)
    elif metodo == 'backtrack':
        return backtrack(problem,
                   variable_heuristic=MOST_CONSTRAINED_VARIABLE,
                   value_heuristic=LEAST_CONSTRAINING_VALUE,
                   inference=True)
    else:
        return None
        
ACCIONES = {
't': 'Sacudir fuertemente la poción por 10 segundos',
'E': 'Mezclar por 5 segundos en el sentido que la Luna gira alrededor de la Tierra',
'm': 'Mezclar por 20 segundos en el sentido de las agujas del reloj',
'l': 'Mezclar por 10 segundos en el sentido que rota la Tierra',
'e': 'Sumergir una manzana cortada por la mitad',
'L': 'Retirar la manzana sumergida',
's': 'Encantar la poción con Stupefy',
'r': 'Encantar la poción con un Sommium',
'O': 'Agregar 2 semillas de Eucaliptus Rojo',
'i': 'Agregar 5 patas de araña',
'v': 'Agregar 500 cm3 de jugo de naranja',
'V': 'Agregar 1 litro de agua',
'd': 'Agregar 50 cm3 de colorante rojo',
'!': 'Tocar la poción con la punta de una pluma',
'o': 'Volcar la mitad del contenido de la poción en el piso'
}

variables = ACCIONES.keys()
dominios = {v:range(15) for v in variables}

INCANTATIONS = list('sr')
MIXES = list('Eml')

BEFORE_AFTER_CONSTRAINTS = []

#0 Todos diferentes.
def all_diff(variables, valores):
    return valores[0] != valores[1]

#1 No se puede sacar la manzana, si antes no se la ha sumergido
BEFORE_AFTER_CONSTRAINTS.append(tuple('eL'))

#2 No se debe agregar ninguna fruta (manzana, naranja) si antes no se han agregado semillas de algun tipo
BEFORE_AFTER_CONSTRAINTS.append(tuple('Ov'))
BEFORE_AFTER_CONSTRAINTS.append(tuple('Oe'))

#3 El último y el primer paso no son un encantamiento
for incantation in INCANTATIONS:
    dominios[incantation].remove(0)
    dominios[incantation].remove(14)
    
#4 Hay que volcar la mitad de la poción antes de llegar a la mitad de los pasos
dominios['o'] = [1, 2, 3, 4, 5, 6]

#5 El encantamiento Sommium tiene que hacerse antes que el encantamiento Stupefy
BEFORE_AFTER_CONSTRAINTS.append(tuple('rs'))

#6 Nunca hay que mezclar en dos pasos que son sucesivos, si no serían un solo pazo de mezcla más larga (sacudir no cuenta como mezclar)
def no_2mix(variables, valores):
    return abs(valores[0] - valores[1]) > 1

#7 Cuando la poción sea sacudida, la manzana tiene que estar dentro
BEFORE_AFTER_CONSTRAINTS.append(tuple('et'))
BEFORE_AFTER_CONSTRAINTS.append(tuple('tL'))

#8 Cuando la manzana esté dentro, no pueden agregarse patas de araña, porque arruinaría el gusto de la manzana
BEFORE_AFTER_CONSTRAINTS.append(tuple('Li'))

#9 La rotación de la Tierra es más importante que la de la Luna, y por eso se mezcla antes usando esa rotación
BEFORE_AFTER_CONSTRAINTS.append(tuple('lE'))

#10 No se puede encantar con Stupefy una poción que no tenga jugo de naranja
BEFORE_AFTER_CONSTRAINTS.append(tuple('vs'))

#11 Nunca puede haber dos componentes frutales en la poción a la vez (manzana y naranja son frutas, las semillas no son frutas)
BEFORE_AFTER_CONSTRAINTS.append(tuple('Lv'))

#12 Para que el toque de pluma tenga efecto, la poción tiene que haber sido encantada con 2 encantamientos
BEFORE_AFTER_CONSTRAINTS.append(tuple('r!'))
BEFORE_AFTER_CONSTRAINTS.append(tuple('s!'))

#13 La poción debe comenzar con un litro de agua antes que ningún otro ingrediente o acción
dominios['V'] = [0]

def before_after(variables, valores):
    return valores[0] < valores[1]


restricciones = []
for step_a, step_b in itertools.combinations(variables, 2):
    restricciones.append(((step_a, step_b), all_diff))

for constraint in BEFORE_AFTER_CONSTRAINTS:
    restricciones.append((constraint, before_after))

for mix_a, mix_b in itertools.combinations(MIXES, 2):
    restricciones.append(((mix_a, mix_b),no_2mix))
