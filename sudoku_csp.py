import itertools
from simpleai.search import (CspProblem, backtrack,
                             MOST_CONSTRAINED_VARIABLE,
                             LEAST_CONSTRAINING_VALUE,
                             HIGHEST_DEGREE_VARIABLE,
                             min_conflicts)

variables = [(i,j) for i in range(9) for j in range(9)]
dominios = {v: [] for v in variables}

INITIAL = '''
25-------
1-4-97-5-
-3652----
--3-8--1-
-186-372-
-4--7-5--
----3219-
-2-14-3-5
-------72
'''

for (fila, columna) in variables:
    num = INITIAL.split()[fila][columna]
    if num == '-':
        dominios[(fila, columna)] = range(1,10)
    else:
        dominios[(fila, columna)] = [int(num)]


def no_lane(variables, valores):
    (fila_a, columna_a), (fila_b, columna_b) = variables
    valor_a, valor_b = valores
    
    if valor_a == valor_b:
        if fila_a == fila_b or columna_a == columna_b:
            return False
    return True

def no_square(variables, valores):
    (fila_a, columna_a), (fila_b, columna_b) = variables
    valor_a, valor_b = valores
    
    if valor_a == valor_b:
        if fila_a / 3 == fila_b / 3 and columna_a / 3 == columna_b / 3:
            return False
    return True

restricciones = []

for num_a, num_b in itertools.combinations(variables, 2):
    restricciones.append(((num_a, num_b), no_lane))
    restricciones.append(((num_a, num_b), no_square))


problem = CspProblem(variables, dominios, restricciones)

########################################################################

print 'Estado inicial:\n'
for l in INITIAL.split():
    print ' '.join(list(l))


#Solucion para comparar:
SOLUTION = '''
259416837
184397256
736528941
573284619
918653724
642971583
465732198
827149365
391865472
'''

print '\nResolviendo min_conflicts...'
result_mc = min_conflicts(problem, iterations_limit=200)

print '\nResultado (min_conflicts):'
for i in range(9):
    print ' '.join([str(result_mc[(i,j)]) for j in range(9)])

print '\nMapa de errores (min_conflicts):'
for i in range(9):
    print ' '.join(['.' if (str(result_mc[(i,j)]) == SOLUTION.split()[i][j]) else 'X' for j in range(9)])

print '\nResolviendo backtrack...'
result_bt = backtrack(problem,
                   variable_heuristic=MOST_CONSTRAINED_VARIABLE,
                   value_heuristic=LEAST_CONSTRAINING_VALUE,
                   inference=True)

print '\nResultado (backtrack):'
for i in range(9):
    print ' '.join([str(result_bt[(i,j)]) for j in range(9)])

print '\nMapa de errores (backtrack):'
for i in range(9):
    print ' '.join(['.' if (str(result_bt[(i,j)]) == SOLUTION.split()[i][j]) else 'X' for j in range(9)])
